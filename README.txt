Armé el proyecto con CMake básicamente porque no se armar makefiles. La idea es que o se usen makefiles o cmake para compilar así es independiente del ide que usemos.
También llené el .gitignore para no agregar basura al repo. Si usan un ide u otra cosa que genere archivos que no van al repo (archivos temporales y binarios), ponen su nombre en el archivo ".gitignore"

Sobre CMake:
Para compilar con CMake, desde esta carpeta (la raiz), escriben 'cmake .' (si están en linux), ó usan el cliente de cmake para windows. Esto les genera un Makefile que compilan con solo "make".
Para agregar archivos al "makefile", agregan a la lista de archivos en CMakeLists.txt, hay algunos comandos en CMake que les deja agregar por directorios, y también les deja trabajar por módulos. Dejé unas lineas comentadas para que se guien.
El que quiere agregar un makefile, bienvenido sea.
