/*
 * Constantes.h
 *
 *  Created on: 25/11/2013
 *      Author: leandro
 */

#ifndef CONSTANTES_H_
#define CONSTANTES_H_

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

#define ERROR -1
#define OK 0

#define REFERENCIA_CARPETA_CLUSTERS "Clusters/"
#define EXTENSION_FILES_TO_PARSE ".txt"
#define EXTENSION_BITMAP ".bit"
#define SHINGLE_LARGE 6
#define KMEANS_MAX_ITERATIONS 500
#define KMEANS_MULTI_MAX_ITERATIONS 100
#define HASHMIN_NORMALIZER 5
#define KMEANS_MULTI_THRESHOLD_INICIAL 5
#define STOP_WORDS "stopWords.txt"
#define CLUSTER_FILE "clusters.clt"

/********** Ctes PARA SHEL PARSER **************/
const short int LIST = 0;
const short int GROUPS = 1;
const short int DIRC = 2;
const short int CANT = 3;
const short int OCUR = 4;
const short int APPEND = 5;
const int CANT_SECUENCIAS = 1000;
/**********************************************/

/********** Ctes PARA el PARSER **************/
static const short int EMPTY_BUFFER = 0;
static const short int READING_WORD = 1;
static const short int READING_NUMBER = 2;

static const short unsigned int MIN_DIGITS_TO_INDEX = 2;
static const short unsigned int MAX_DIGITS_TO_INDEX = 10;

static const short int WHITESPACE = 0;
static const short int DIGIT = 1;
static const short int LETTER = 2;
static const short int TITTLE = 3;
static const short int OTHER = 4;
#endif /* CONSTANTES_H_ */
/**********************************************/
