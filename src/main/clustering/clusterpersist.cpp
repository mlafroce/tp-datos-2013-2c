#include "clusterpersist.h"
#include <cstdio>

/**
 * Primero escribo la cantidad de clusters, luego, por cluster
 * escribo la cantidad de hashmins y a continuación el nombre y
 * tamaño del string. Por último escribo el tamaño del vector
 * values y los valores del 
 */
void ClusterPersist::persist(FILE* file, std::vector<Cluster> &clusters)
{
	int aux =clusters.size();
	fwrite(&aux, sizeof(int), 1, file);
	for (int i = 0; i< clusters.size(); i++){
		aux = clusters[i].getSize();
		fwrite(&aux, sizeof(int), 1, file);
		std::vector<Hashmin> hashmins;
		hashmins = clusters[i].getHashmins();
		for (int j=0; j < hashmins.size(); j++){
			std::string name = hashmins[j].name;
			aux = name.length();
			fwrite(&aux, sizeof(int), 1, file);
			fwrite(name.c_str(), sizeof(char), aux, file);
			std::vector<int> hashminValues = hashmins[j].values;
			aux = hashminValues.size();
			fwrite(&aux, sizeof(int), 1, file);
			//Hack que encontré por ahí...
			fwrite(&hashminValues[0], sizeof(int), hashminValues.size(), file);
		}
	}
}

std::vector<Cluster> ClusterPersist::load(FILE* file)
{
	int clusterAmmount;
	std::vector<Cluster> clusters;
	fread(&clusterAmmount, sizeof(int), 1, file);
	for (;clusterAmmount > 0; clusterAmmount--){
		int hashminsAmmount;
		std::vector<Hashmin> hashmins;
		fread(&hashminsAmmount, sizeof(int), 1, file);
		for (;hashminsAmmount>0; hashminsAmmount--){
			std::vector<int> hashminValues;
			//Levanto el nombre
			int nameLength;
			//Otro superhack
			fread(&nameLength, sizeof(int), 1, file);
			std::string name;
			name.resize(nameLength);
			fread(&name[0], sizeof(char), nameLength, file);
			int valuesAmmount;
			fread(&valuesAmmount, sizeof(int), 1, file);
			std::vector<int> values(valuesAmmount);
			fread(&values[0], sizeof(int), valuesAmmount, file);
			Hashmin h(name, values);
			hashmins.push_back(h);
		}
		Cluster clusterAux(hashmins);
		clusters.push_back(clusterAux);
	}
	return clusters;
}
