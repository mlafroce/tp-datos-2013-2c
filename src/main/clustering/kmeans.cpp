#include "kmeans.h"
#include "cluster.h"
#include "../hashmin/secuenciarandom.h"
#include <cmath>
#include <ctime>
#include <iostream>
#include <string.h>

Kmeans::Kmeans(std::vector< Hashmin > &hashmins, int totalClusters)
{
	this->hashmins = hashmins;
	if (totalClusters == -1){
		if (hashmins.size() < 3)
			totalClusters = 2;
		else if (hashmins.size() < 150)
			totalClusters = sqrt(hashmins.size());
		else totalClusters = log(hashmins.size());
	}
	this->totalClusters = totalClusters;
}

std::vector<Cluster>  Kmeans::clusterize(int maxIterations, bool multiple)
{
	std::vector<Cluster> clusters;
	centroids = getInitialCentroids(this->hashmins);
	if (multiple){
		clusters = clusterizeMultiple(maxIterations);
	} else {
		clusters = clusterizeExclusive(maxIterations);
	}
	return clusters;
}

std::vector<Cluster> Kmeans::clusterizeExclusive(int maxIterations){
	std::vector<Cluster> clusters;
	int k = this->centroids.size();
	int i = 0;
	for (i; i < maxIterations -1; i++){
		clusters = createKClusters(this->centroids);
		this->centroids.clear();
		for (int j = 0; j < k; j++){
			Hashmin centroid = clusters.at(j).getCentroid();
			this->centroids.push_back(centroid);
		}
	}
	clusters = createKClusters(this->centroids);
	return clusters;
}

std::vector<Hashmin> Kmeans::getInitialCentroids(int k)
{

	std::vector<int> seq;
	for (int i = 0; i < this->hashmins.size();i++){
		seq.push_back(i);
	}
	std::vector<Hashmin> centroids;
	SecuenciaRandom sr (this->hashmins.size(), (unsigned)time(NULL));
	for (int i=0; i<k; i++) {
		centroids.push_back(this->hashmins[sr.getNext()]);
	}
	return centroids;
}

std::vector<Cluster> Kmeans::createKClusters(std::vector<Hashmin> &centroids){
	std::vector<Cluster> clusters(centroids.size());
	for (int i = 0; i < this->hashmins.size(); i++){
		double maxDistance = 0;
		int clusterId = 0;
		bool foundMyself = false;
		//Busco el centroide mas cercano
		for (int j = 0; j < centroids.size(); j++){
			double curDistance = this->hashmins.at(i).distance(centroids.at(j));
			if (curDistance >= maxDistance){
				if (!foundMyself) {
					maxDistance = curDistance;
					clusterId = j;
				}
				if (0 == strcmp(this->hashmins.at(i).name.c_str(), centroids.at(j).name.c_str())){
					foundMyself = true;
				}
			}
		}
		clusters[clusterId].add(hashmins[i]);
	}
	return clusters;
}

std::vector<Hashmin> Kmeans::getInitialCentroids(std::vector<Hashmin> hashmins) {
	std::vector<Hashmin> centroides;
	centroides.push_back(this->hashmins.at(0));
	hashmins.erase(hashmins.begin());
	double distanciaMinima = 0.0;
	int index;
	for (int i = 1; i < this->totalClusters; i++) {
		//Hashmin centroide = centroides.at(centroides.size() - 1);
		distanciaMinima = 2.0;
		for (int j = 0; j < hashmins.size(); j++) {
			double distancia = 0.0;
			for (int k = 0; k < centroides.size(); k++) {
				distancia += centroides.at(k).distance(hashmins.at(j));
			}
			if ((distancia / centroides.size()) < distanciaMinima) {
				distanciaMinima = distancia;
				index = j;
			}
		}
		centroides.push_back(hashmins.at(index));
		hashmins.erase(hashmins.begin() + index);
	}
	return centroides;

}

std::vector<Cluster> Kmeans::clusterizeMultiple(int maxIterations)
{
	std::map< std::string, std::set< int > > mapaHashmins;
    int k = this->centroids.size();
	std::vector<Cluster> clusters;
	//Primero entreno un poco los clusters
	for (int i = 0; i < KMEANS_MULTI_THRESHOLD_INICIAL; i++){
		clusters = createKClusters(this->centroids);
		this->centroids.clear();
        for (int j = 0; j < k; j++){
			Hashmin centroid = clusters.at(j).getCentroid();
			this->centroids.push_back(centroid);
		}
	}
	//Ahora lleno el set con los clusters en los que fue apareciendo cada hashmin
	for (int i = 0; i < maxIterations; i++){
		clusters = createKClusters(this->centroids);
		this->centroids.clear();
        for (int j = 0; j < k; j++){
			Cluster cl = clusters.at(j);
			std::vector<Hashmin> hashminsCl = cl.getHashmins();
			Hashmin centroid = cl.getCentroid();
			for (int hm = 0; hm < hashminsCl.size(); hm++){
				Hashmin clave = hashminsCl[hm];
				//Inserto el número de cluster
				mapaHashmins[clave.name].insert(j);
			}
			this->centroids.push_back(centroid);
		}
	}
	//Ahora limpio los clusters
	for (int i = 0; i < this->centroids.size(); i++){
		clusters[i].clear();
	}
	std::map< std::string, std::set< int > >::iterator keyIter = mapaHashmins.begin();
	while (keyIter != mapaHashmins.end()){
		std::string name = keyIter->first;
		Hashmin hashmin = getHashminByName(name);
		std::set<int>::iterator setIter = mapaHashmins[keyIter->first].begin();
		while (setIter != mapaHashmins[keyIter->first].end()){
			clusters[*setIter].add(hashmin);
			setIter++;
		}
		keyIter++;
	}
	return clusters;
}

Hashmin Kmeans::getHashminByName(std::string& name)
{
	for (int i = 0; i < this->hashmins.size(); i++){
		if (this->hashmins[i].name.compare(name) == 0){
			return this->hashmins[i];
		}
	}
}
