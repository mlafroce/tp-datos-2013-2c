#ifndef CLUSTER_H
#define CLUSTER_H
#include "../hashmin/hashmin.h"
#include <vector>
#include <iostream>

class Cluster
{
public:
	Cluster();
	Cluster(std::vector<Hashmin> &hashmins);
	/**
	 * La "distancia" entre hashmins se mide según la cantidad de términos en común que tienen,
	 * Mientras menor sea el número, mas lejos se encuentran
	 */
	Hashmin getCentroid();
	void add(Hashmin &hashmin);
	void clear();
	int getSize();
	std::vector<Hashmin> getHashmins();
private:
	std::vector<Hashmin> hashmins;
};

#endif // CLUSTER_H
