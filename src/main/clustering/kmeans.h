#ifndef KMEANS_H
#define KMEANS_H
#include <vector>
#include <map>
#include <set>
#include "../hashmin/hashmin.h"
#include "cluster.h"

class Kmeans
{
public:
	Kmeans(std::vector<Hashmin> &hashmins, int k);
	std::vector<Cluster> clusterize(int maxIterations, bool multiple);
	std::vector<Cluster> clusterizeMultiple(int maxIterations);
	void addHashmin(Hashmin &hashmin);

private:
	std::vector<Hashmin> hashmins;
	std::vector<Hashmin> centroids;
	int totalClusters;
	std::vector< Hashmin > getInitialCentroids(int k);
	std::vector< Cluster > createKClusters(std::vector< Hashmin > &centroids);
	std::vector<Cluster> clusterize(std::vector<Hashmin> &centroids, int iterations);
	std::vector<Hashmin> getInitialCentroids(std::vector<Hashmin> hashmins);
	std::vector<Cluster> clusterizeExclusive(int iterations);
	Hashmin getHashminByName(std::string &name);
};

#endif // KMEANS_H
