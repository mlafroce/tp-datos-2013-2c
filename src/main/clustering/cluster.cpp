#include "cluster.h"

Cluster::Cluster(std::vector< Hashmin > &hashmins)
{
	this->hashmins = hashmins;
}

Cluster::Cluster()
{
}

void Cluster::add(Hashmin &hashmin)
{
	this->hashmins.push_back(hashmin);
}

void Cluster::clear()
{
	this->hashmins.clear();
}

Hashmin Cluster::getCentroid()
{
	//Como 0 significa que no hay parecido, lo uso como distancia inicial
	double closestDistance = 0.0;
	int index = 0;
	int totalHashmins = this->hashmins.size();
	for(int i = 0; i< totalHashmins; i++){
		double currentDistance = 0.0;
		//La distancia promedio no da buen resultado
		for(int j = 0; j< totalHashmins; j++){
			if (i != j){
				double aux = this->hashmins[i].distance(this->hashmins[j]);
				currentDistance += aux;
			}
		}
		currentDistance = currentDistance / totalHashmins;
		//Si es mas cercano... Distance no es el nombre mas feliz para esta operación :P
		if (currentDistance > closestDistance){
			index = i;
			closestDistance = currentDistance;
		}
	}
	Hashmin result = this->hashmins[index];
	return result;
}

int Cluster::getSize()
{
	return this->hashmins.size();
}

std::vector< Hashmin > Cluster::getHashmins()
{
	return this->hashmins;
}
