#ifndef CLUSTERPERSIST_H
#define CLUSTERPERSIST_H
#include <vector>
#include <map>
#include <set>
#include "cluster.h"

class ClusterPersist
{

public:
	static void persist(FILE* file, std::vector<Cluster> &clusters);
	static std::vector<Cluster> load(FILE* file);
};

#endif // CLUSTERPERSIST_H
