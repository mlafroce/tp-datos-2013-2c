/*
 * Matrix.cpp
 *
 *  Created on: 17/11/2013
 *      Author: agustin
 */
#include "Matrix.h"


Matrix::Matrix(){}

Matrix::~Matrix(){}

void Matrix::initialize(int s, int k){
	int i,j;
	for (i=0; i<s; i++){
		vector<int> aux;
		for (j=0; j<k; j++){
			aux.push_back(j);
		}
		sortRandomVector(aux, i);
		matrix.push_back(aux);
	}

}

vector<vector<int> > Matrix::getMatrix(){
	return matrix;
}

int myRandom (int i) {
	return rand()%i;
}

void Matrix::sortRandomVector(vector<int> &myVector, int i){
	srand (i);
	// using myRandom:
	random_shuffle (myVector.begin(),myVector.end(), myRandom);
}



