#ifndef BITMAP_H
#define BITMAP_H
#include <vector>
#include <string>
#include "bitwriter.h"
#include "bitreader.h"
#include "../Utils/Constantes.h"

class Bitmap
{
public:
	Bitmap(std::string fileName);
	void addPosition(int pos);
	int getPosition(int pos);
	int size();
	int persist();
	int loadBitmap();
	std::string getName();
	void setName(std::string);
private:
	std::vector<bool> bits;
	std::string  fileName;
};

#endif // BITMAP_H
