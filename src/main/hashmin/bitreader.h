/*
    
*/


#ifndef BITREADER_H
#define BITREADER_H

class BitReader
{

public:
  BitReader();
  virtual ~BitReader();
  void cargarCadena(char* cadena, int largo);
  bool leerBit();
  
private:
  unsigned int offset;
  unsigned int largo;
  unsigned char mask;
  char* cadena;
};

#endif // BITREADER_H
