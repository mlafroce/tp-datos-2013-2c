#include "hashmin.h"
#include "../Utils/Constantes.h"
#include <iostream>

Hashmin::Hashmin(std::vector<SecuenciaRandom> &sr, Bitmap &bitmap)
{
	int srSize = sr.size();
	this->values.resize(srSize);
	for (int i = 0; i < srSize; i++){
		int mh = minhash(sr[i], bitmap);
		this->values[i] = mh;
	}
	this->name = bitmap.getName();
}

Hashmin::Hashmin(std::string &name, std::vector< int > &values)
{
	this->name = name;
	this->values = values;
}


int Hashmin::minhash(SecuenciaRandom &sr, Bitmap &bitmap)
{
	sr.reset();
	bool found = false;
	int i = 0;
	char normalizerCounter = 0;
	while ((!found || normalizerCounter < HASHMIN_NORMALIZER ) && sr.hasNext()){
		found = bitmap.getPosition(sr.getNext());
		i++;
		if (found){
			normalizerCounter++;
		}
	}
	return i;
}

double Hashmin::distance(Hashmin &hashmin)
{
	std::string name = this->name;
	double intersectCount = 0;
	int valuesSize = this->values.size();
	for (int i=0; i< valuesSize; i++){
		if (this->values[i] == hashmin.values[i]){
			intersectCount++;
		}
	}
	double distance = intersectCount/this->values.size();
	return distance;
}

