#include "bitmap.h"
#include <iostream>
#include <cstring>
#include <cstdio>
#include <fstream>
#include "../Utils/Constantes.h"

Bitmap::Bitmap(std::string fileName) {
	this->fileName = fileName;
}

void Bitmap::addPosition(int pos) {
	if (this->bits.size() <= pos) {
		this->bits.resize(pos + 1);
	}
	this->bits[pos] = true;
}

int Bitmap::getPosition(int pos) {
	if (pos >= this->bits.size()) {
        return 0;
	}
	return this->bits.at(pos);
}

void Bitmap::setName(std::string filename) {
	this->fileName = filename;
}

std::string Bitmap::getName() {
	return this->fileName;
}

int Bitmap::size() {
	return this->bits.size();
}

int Bitmap::persist() {
	std::ofstream* output;
	std::string file = REFERENCIA_CARPETA_CLUSTERS + fileName + EXTENSION_BITMAP;
	output = new std::ofstream(file.c_str(), std::ios::out | std::ios::binary);

	if (output == NULL) {
		printf("Error al crear archivo de bitmap\n");
		return ERROR;
	}

	BitWriter writer;
	writer.crearBuffer(bits.size() / 8 + 1);

	for (int i = 0; i < bits.size(); i++) {
		writer.grabarBit(bits[i]);
	}
	char* cadena = writer.obtenerCadena();

	int aux = bits.size() / 8 + 1;
	output->write(reinterpret_cast<char*>(&aux), sizeof(int));
	output->write(cadena, bits.size() / 8 + 1);
	output->close();
	delete output;

	return OK;
}

int Bitmap::loadBitmap() {
	BitReader reader;
	std::ifstream* input = new std::ifstream(fileName.c_str(), std::ios::in | std::ios::binary);
	if (input == NULL) {
		printf("Error al abrir archivo de bitmap\n");
		return ERROR;
	}

	int tam;
	input->read(reinterpret_cast<char*>(&tam), sizeof(int));
	char* cadena = new char[tam];
	for (int i=0; i<tam; i++)
		cadena[i] = 0;

	input->read(cadena, tam);

	reader.cargarCadena(cadena, tam);

	for (int i = 0; i < tam * 8; i++) {
		this->bits.push_back(reader.leerBit());
	}

	input->close();
	delete input;

	return OK;
}
