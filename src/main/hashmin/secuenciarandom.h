#ifndef SECUENCIARANDOM_H
#define SECUENCIARANDOM_H
#include <vector>
#include <algorithm>    // std::random_shuffle
#include <cstdlib>      // std::rand, std::srand

class SecuenciaRandom
{
public:
	SecuenciaRandom(int size, int seed);
	int getNext();
	bool hasNext();
	void reset();
protected:
	std::vector<int> seq;
	std::vector<int>::iterator it;
};

#endif // SECUENCIARANDOM_H
