/*
 * created on: 25/11/2013
 * Author: Rodrigo
 * Choriated of: Mati
*/


#ifndef BITWRITER_H
#define BITWRITER_H

class BitWriter
{
public:
  BitWriter();
  ~BitWriter();
  void crearBuffer(int size);
  void grabarBit(bool bit);
  char* obtenerCadena();
private:
  char* cadena;
  char mask;
  int posicion;
 
};

#endif // BITWRITTER_H
