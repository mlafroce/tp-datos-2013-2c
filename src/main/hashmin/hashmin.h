#ifndef HASHMIN_H
#define HASHMIN_H
#include <vector>
#include "secuenciarandom.h"
#include "bitmap.h"

class Hashmin
{
public:
	Hashmin(std::vector<SecuenciaRandom> &sr, Bitmap &bitmap);
	Hashmin(std::string &name, std::vector<int> &values);
	double distance(Hashmin &hashmin);
	std::vector<int> values;
	std::string name;
private:
	int minhash (SecuenciaRandom &sr, Bitmap &bitmap);
};

#endif // HASHMIN_H
