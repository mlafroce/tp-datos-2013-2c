#include "secuenciarandom.h"

int myRandom (int i);

SecuenciaRandom::SecuenciaRandom(int size, int seed)
{
	this->seq = std::vector<int>(size);
	this->it = this->seq.begin();
	int i;
	for (i=0; i<size; i++) {
		seq[i] = i;
	}
	srand (seed);
	// using myRandom:
	random_shuffle (this->seq.begin(),this->seq.end(), myRandom);
}

bool SecuenciaRandom::hasNext()
{
 	return this->it != seq.end();
}

int SecuenciaRandom::getNext()
{
	int value = *(this->it);
	this->it++;
	return value;
}

int myRandom (int i) {
	return rand()%i;
}

void SecuenciaRandom::reset()
{
	this->it = this->seq.begin();
}