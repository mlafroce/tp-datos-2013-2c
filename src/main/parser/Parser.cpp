
#include "Parser.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <cstring>
#include <time.h>
#include <iostream>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using std::ifstream;
using std::fstream;

short int Parser::getCharClass(char c) {
	int auxC = (int) c;
	if ( ('A' <= c && c <= 'Z') || //mayusculas
			('a' <= c && c <= 'z') ) { // minusculas
		return LETTER;
	} else if ( (8 <= auxC && auxC <= 13) ||  // all tabs
			(auxC == 32)) { // 32 = space
		return WHITESPACE;
	} else if ('0' <= c && c <= '9') {
		return DIGIT;
	} else if (auxC == -61) {
		return TITTLE;
	} else {
		return OTHER;
	}
}

Parser::Parser(vector <string>* v_stopWords) {
	setState(EMPTY_BUFFER);
	shingleNumber = 0;
	this->v_stopWords = v_stopWords;
}

void Parser::parseDocument(char*  filename) {
	buffer = "";

	ifstream stream;
	stream.open(filename, fstream::in);

	char c;
	short int tipoChar;
	short int nextState = EMPTY_BUFFER;
	this->setState(EMPTY_BUFFER);

	if (!stream.is_open()) {
		cout << "No se pudo abrir el archivo " << filename << endl;
		stream.clear();
		stream.close();
		return;
	}

	if (!stream.good()) {
		cout << "El archivo " << filename << " se encuentra tan corrupto como Argentina.\n";
		stream.clear();
		stream.close();

		return;
	}

	while (!stream.eof()) {

		stream.get(c);

		if( !stream.eof()) { // miro de nuevo si llegue al final del archivo

			tipoChar = getCharClass(c);
			if (tipoChar == TITTLE) {
				// los tildes se graban -61 y después otro caracter con la vocal
				// o sea ocupan 2 bytes y tengo que leer 2 seguidos.
				stream.get(c);
				c =	removeTittle(c);
				tipoChar = getCharClass(c);
			}
		} else {

			switch (getState()) {
			case READING_WORD:
				indexWord();
				nextState = EMPTY_BUFFER;
				break;
			case READING_NUMBER:
				indexNumber();
				nextState = EMPTY_BUFFER;
				break;
			}
		}

		switch (getState()) {
		case EMPTY_BUFFER:
			switch (tipoChar) {
			case LETTER:
				appendLetter(c);
				nextState = READING_WORD;
				break;
			case DIGIT:
				buffer.push_back(c);
				nextState = READING_NUMBER;
				break;
			default:
				break;
			}
			break;
		case READING_WORD:
			switch (tipoChar) {
			case LETTER:
				appendLetter(c);
				break;
			case DIGIT:
				buffer.push_back(c);
				break;
			case WHITESPACE:
			default:
				indexWord();
				nextState = EMPTY_BUFFER;
				break;
			}
			break;
		case READING_NUMBER:
			switch(tipoChar){
			case DIGIT:
				buffer.push_back(c);
				break;
			case LETTER:
				appendLetter(c);
				nextState = READING_WORD;
				break;
			case WHITESPACE:
			default:
				indexNumber();
				nextState = EMPTY_BUFFER;
				break;
			}
			break;
		}

		if ( nextState != getState())
			this->setState(nextState);
	}
	stream.clear();
	stream.close();
}

bool Parser::findStopWord(string st) {
	for (unsigned int i=0; i<v_stopWords->size(); i++) {
		if (st == v_stopWords->at(i)) return true;
	}
	return false;
}

void Parser::indexWord() {
	if (v_stopWords != NULL)
		if (findStopWord(buffer)) {
//			printf("hallo");
			return;
		}
	
	shinglesList += buffer;
}

void Parser::indexNumber () {
	if (digitsIndexables(getBuffer())) {
		shinglesList += buffer;
	}
}

string Parser::getShingle() {
	if ((int)shinglesList.length()+1 == shingleNumber+SHINGLE_LARGE)
		return "";
	string sh = shinglesList.substr(shingleNumber, SHINGLE_LARGE);
	shingleNumber++;
	return sh;
}

void Parser::appendLetter (char c) {
	c = tolower(c);
	buffer.push_back(c);
}

bool Parser::digitsIndexables(string number) {
	return ( (MIN_DIGITS_TO_INDEX <= number.length()) && (number.length() <= MAX_DIGITS_TO_INDEX) );
}

char Parser::removeTittle(char c) {
	int auxC = (int) c;
	switch (auxC) {
	case -127:
	case -95:
		return 'a';
		break;
	case -119:
	case -87:
		return 'e';
		break;
	case -83:
	case -115:
		return 'i';
		break;
	case -77:
	case -109:
		return 'o';
		break;
	case -70:
	case -102:
		return 'u';
		break;
	case -79:
	case -111:
		return 'n';
		break;
	}
	return c;
}

void Parser::setState(short int newState) {
	this->state = newState;

	if (state == EMPTY_BUFFER) {
	    buffer.clear();
	    buffer = "";
	}
}

short int Parser::getState() {
	return state;
}

void Parser::setBuffer(string buff) {
  this->buffer = buff;
}

string Parser::getBuffer() {
  return this->buffer;
}

Parser::~Parser() {
}
