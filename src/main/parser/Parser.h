
#ifndef PARSER_H_
#define PARSER_H_

#include <fstream>
#include <string>
#include <vector>
#include "../Utils/Constantes.h"

using std::string;
using std::ofstream;
using std::vector;

class Parser {
private:
	short int state;
	string buffer;
	string shinglesList;
	int shingleNumber;
	vector <string>* v_stopWords;

	void indexWord();
	void indexNumber();

	bool findStopWord(string);
	
	bool digitsIndexables(string);
	void appendLetter(char);
	char removeTittle(char);
	short int getCharClass(char);
public:
	void parseDocument(char*);

	short int getState();
	void setState(short int);
	string getBuffer();
	void setBuffer(string);

	string getShingle();

	Parser(vector <string>*);
	virtual ~Parser();
};

#endif /* PARSER_H_ */
