
#ifndef SHELLPARSER_H_
#define SHELLPARSER_H_

#include "Parser.h"
#include "../hashmin/bitmap.h"
#include "../hashmin/hashmin.h"
#include "../shinglesList/ShingleList.h"
#include "../clustering/kmeans.h"
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

using std::string;

class ShellParser {
private:
	string repo;
	string rutaDelRepo;
	void printArgument(char, char*);
	void crearCarpetaAlmacenamientoClusters(string&);
	void getFileName(char*, char*, char*, int);
	void printListados(short int);
	void removeOldClusters(DIR*, char*);
	void clasificar(char*);
	
public:
	ShellParser();
	virtual ~ShellParser();
	void help();
	void version();
	int getOptions(int argc, char** argv);
};

#endif /* SHELLPARSER_H_ */
