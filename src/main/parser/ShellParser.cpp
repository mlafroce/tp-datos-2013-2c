#include "ShellParser.h"
#include "../Listar/ListaArchivos.h"
#include <map>
#include <set>
#include <getopt.h>
#include <stdio.h>
#include <cstdio>
#include <stdlib.h>
#include <vector>
#include <iostream>

using std::cout;
using std::endl;
using std::exception;
using std::vector;

static struct option long_options[] = {
		{ "help", 		0, NULL, 'h' },
		{ "list", 		0, NULL, 'l' },
		{ "groups", 		0, NULL, 'g' },
		{ "append", 		1, NULL, 'a' },
		{ "cantidad", 		1, NULL, 'c' },
		{ "dir", 		1, NULL, 'd' },
		{ "ocurrences",		1, NULL, 'o' },
		{  0, 			0, NULL,  0  }
};
const char* modifiers = "hlgacdo";

ShellParser::ShellParser() {
}

vector <string>* getUpStopWords() {
	ifstream file;
	file.open(STOP_WORDS, std::ifstream::in);

	if (file == NULL) {
		printf ("No existe archivo de StopWords.\nSe utilizaran todas las palabras.\n");
		return NULL;
	}

	printf ("Hallado archivo StopWords con ");
	char buffer [20];
	vector <string>* v_stopWords = new vector<string>();

	while (!file.eof()) {
		file.getline(buffer, 20);
		string sw(buffer);
		v_stopWords->push_back(sw);
	}
	printf ("%d palabras\n", v_stopWords->size());
	return v_stopWords;
}

void ShellParser::printListados(short int option) {

	string d_name = REFERENCIA_CARPETA_CLUSTERS CLUSTER_FILE;
	FILE* file = fopen(d_name.c_str(), "r");
	vector<Cluster> v_clusters = ClusterPersist::load(file);
	ListaArchivos listaArchivo;
	if (option == LIST) {
		listaArchivo.initialize(v_clusters);
		listaArchivo.leerDocumentosOrdenados(v_clusters);
	} else if (option == GROUPS) {
		listaArchivo.leerClustersCorrido(v_clusters);
	} else return;
}

void ShellParser::getFileName(char n_file[], char* dir, char* file, int tam) {
	for (int i=0; i<tam; i++) n_file[i] = 0;
	strcat(n_file, dir);
	strcat(n_file, "/");
	strcat(n_file, file);
}

void ShellParser::removeOldClusters(DIR* dir, char* rutaDir) {
	struct dirent* entry;
	
	while (entry = readdir(dir)) {
		if ((strcmp(entry->d_name,".")!= 0) && (strcmp(entry->d_name,"..") != 0)) {
			int tam = 256;
			char n_name [tam];
			getFileName(n_name, rutaDir, entry->d_name, tam);
			
			if (remove(n_name) != 0)
				printf ("Error al eliminar el archivo: %s\n", n_name);
		}
	}
}

string rescatarNombre (char* file) {
	string nombre = file;
	string salida = "";

	int count = 0;
	for (int i=0; i<nombre.length(); i++) {
		salida.append(nombre, i, 1);
		if (nombre[i] == '/')
			salida.clear();
	}
	return salida;
}

void ShellParser::clasificar (char* file) {
	ShingleList* shinglesList = new ShingleList();
	printf ("\nCargando shingles...\n");
	shinglesList->loadShingleList();

	vector <string>* v_stopWords = getUpStopWords();
	Parser* parser = new Parser(v_stopWords);
	Bitmap n_bitmap (file);
	printf ("\nParseando documento...\n");
	parser->parseDocument(file);

	printf ("Generando bitmap...\n");
	string strShingle = parser->getShingle();
	while (strShingle != "") {
		char* charShingle = new char [strShingle.length() + 1];
		strcpy (charShingle, strShingle.c_str());

		int pos = shinglesList->addShingle(charShingle);
		n_bitmap.addPosition(pos);
		strShingle = parser->getShingle();

		delete charShingle;
	}

	n_bitmap.setName(rescatarNombre(file));
	n_bitmap.persist();
	int shingleListSize = shinglesList->size();
	shinglesList->persistShingleList();
	printf ("\nLista de shingles persistida!\n Hay %d shingles\n", shingleListSize);
	delete parser;
	delete shinglesList;
	delete v_stopWords;

	printf("\nCalculando secuencias randoms...\n");
	vector<SecuenciaRandom> v_secRand;
	for (int i=0; i<CANT_SECUENCIAS; i++) {
        SecuenciaRandom sr (shingleListSize, i);
		v_secRand.push_back(sr);
	}

	printf("Obteniendo bitmaps...\n");
	vector<Bitmap> v_bitmap;
	DIR* dir = opendir(REFERENCIA_CARPETA_CLUSTERS);
	struct dirent* entryB;
	while (entryB = readdir(dir)) {
		if ((strcmp(entryB->d_name, ".") != 0) && (strcmp(entryB->d_name, "..") != 0)) {
			if (strstr(entryB->d_name, EXTENSION_BITMAP)) {
				int tam = 256;
				char n_name[tam];
				char c [sizeof(REFERENCIA_CARPETA_CLUSTERS) + 1];
				strcpy (c, REFERENCIA_CARPETA_CLUSTERS);
				getFileName(n_name, c, entryB->d_name, tam);
				Bitmap bitmap (n_name);
				bitmap.loadBitmap();
				v_bitmap.push_back(bitmap);
			}
		}
	}

	printf("Obteniendo hashmins...\n");
	vector<Hashmin> v_hashmin;
	for (unsigned int i=0 ; i<v_bitmap.size(); i++) {
		Hashmin hashmin (v_secRand, v_bitmap.at(i));
		v_hashmin.push_back(hashmin);
	}
	//Calculo el hashmin del bitmap del nuevo doc.
	string name_final = "/" + n_bitmap.getName();
	name_final = REFERENCIA_CARPETA_CLUSTERS + name_final + EXTENSION_BITMAP;
	n_bitmap.setName(name_final);
	Hashmin n_hashmin (v_secRand, n_bitmap);

	printf("Levantando clusters...\n");
	string d_name = REFERENCIA_CARPETA_CLUSTERS CLUSTER_FILE;
	FILE* c_file = fopen(d_name.c_str(), "r");
	vector<Cluster> v_clusters = ClusterPersist::load(c_file);
	vector<Hashmin> centroids;
	for (int i=0; i<v_clusters.size(); i++) {
		centroids.push_back(v_clusters.at(i).getCentroid());
	}

	// Busco de estos centroides los nuevos hashmins y comparo la distancia
	// con el nuevo doc a ver a cual pertenece
	printf("Calculando distancia...\n");
	double distance = 0;
	int centroideId = 0;
	for (int i=0; i<centroids.size(); i++) {
		for (int j=0; j<v_hashmin.size(); j++) {
			if (centroids.at(i).name == v_hashmin.at(j).name) {
				double distanceAux = n_hashmin.distance(v_hashmin.at(j));
				if (distance <= distanceAux) {
					distance = distanceAux;
					centroideId = i;
				}
			}
		}
	}
	string hashminGanador = centroids.at(centroideId).name;
	bool agregado = false;

	//Vuelvo a armar los clusters con el nuevo doc
	printf("Añadiendo documento nuevo al cluster...\n");
	vector<Cluster> v_nuevosClusters;
	for (int i=0; i<v_clusters.size(); i++) {
		Cluster cluster;
		vector<Hashmin> oldHashmins = v_clusters.at(i).getHashmins();
		for (int j=0; j<oldHashmins.size(); j++) {
			for (int k=0; k<v_hashmin.size(); k++) {
				if (oldHashmins.at(j).name == v_hashmin.at(k).name) {
					cluster.add(v_hashmin.at(k));
					if (v_hashmin.at(k).name == hashminGanador && !agregado) {
						cluster.add(n_hashmin);
						agregado = true;
					}
				}
			}
		}
		v_nuevosClusters.push_back(cluster);
	}
	fclose(c_file);
	c_file = fopen(d_name.c_str(), "w");
	ClusterPersist::persist(c_file, v_nuevosClusters);
	printf ("Hecho\n");
}

int ShellParser::getOptions(int argc, char** argv) {
	if (argc < 1) {
		cout << "Pocos parámetros." << endl;
		help();
		return EXIT_FAILURE;
	}

	int c;
	vector<short int> *opciones = new vector<short int>();

	string repo = string("");
	char* buffer = (char*)"";
	char* ocur = (char*)"";
	short int cant = 0;

	while ((c = getopt_long(argc, argv, modifiers, long_options, NULL)) != EOF) {

		switch (c) {
		case 'h':
			this->help();
			delete opciones;
			return EXIT_SUCCESS;
			break;
		case 'l':
			opciones->push_back(LIST);
			break;
		case 'g':
			opciones->push_back(GROUPS);
			break;
		case 'a':
			opciones->push_back(APPEND);
			buffer = optarg;
			break;
		case 'd':
			opciones->push_back(DIRC);
			buffer = optarg;
//			printf("d: %s\n", buffer);
			break;
		case 'c':
			opciones->push_back(CANT);
			if ((atoi(optarg) >= 1) && (atoi(optarg) <= 100))
				cant = atoi(optarg);
			else cant = -1;
//			printf("c: %d\n", cant);
			break;
		case 'o':
			opciones->push_back(OCUR);
			ocur = optarg;
//			printf("o: %s\n", ocur);
			break;
		case '?':
			cout << "Parámetro inexistente, si seguís escribiendo así te vas a ir a la B como Riber e Independiente.\n\n";
			help();
			delete opciones;
			return EXIT_FAILURE;
			break;
		default:
			help();
			delete opciones;
			return EXIT_SUCCESS;
			break;
		}
	}

	if (opciones->size() == 2 || opciones->size() == 3) {
		ShingleList* shinglesList = new ShingleList();
		struct dirent * entry;
		DIR* directory = opendir(buffer);
		DIR* clustersDir = opendir(REFERENCIA_CARPETA_CLUSTERS);

		printf ("\nEjecutando entrenamiento...\n");
		
		// Chequeo la ruta del repo ingresada
		if (directory == NULL) {
			printf ("ERROR: La ruta del repositorio ingresada es invalida.\n");
			return ERROR;
		}
		
		// Chequeo que no exista la carpeta Clusters (sino la borro asi clusteriza de cero
		if (clustersDir != NULL) {
			char c [sizeof(REFERENCIA_CARPETA_CLUSTERS) + 1];
			strcpy (c, REFERENCIA_CARPETA_CLUSTERS);
			removeOldClusters(clustersDir, c);
			closedir(clustersDir);
		} else { // Creamos la carpeta CLUSTERS
			int status = mkdir(REFERENCIA_CARPETA_CLUSTERS, S_IFDIR | S_IRWXU | S_IRWXO);
			if (status != 0) {
				printf ("Error al crear carpeta de clusters!\n");
				return ERROR;
			}
		}
		
		int documentos = 0;
		vector <string>* v_stopWords = getUpStopWords();

		while( (entry = readdir(directory)) ) {
			if( (strcmp(entry->d_name,".")!= 0) && (strcmp(entry->d_name,"..") != 0) && (strstr(entry->d_name,"~") == NULL)) {
				if(strstr(entry->d_name, EXTENSION_FILES_TO_PARSE)!= NULL ) {
					int tam = 128;
					char n_file[tam];
					getFileName(n_file, buffer, entry->d_name, tam);
					
					Parser* parser = new Parser(v_stopWords);
					printf ("\nParseando documento: %s\n", n_file);
					parser->parseDocument(n_file);
					Bitmap* bitmap = new Bitmap(entry->d_name);
					
					printf ("Generando bitmap...\n");
					string strShingle = parser->getShingle();
					while (strShingle != "") {
						char* charShingle = new char [strShingle.length() + 1];
						strcpy (charShingle, strShingle.c_str());

						int pos = shinglesList->addShingle(charShingle);
						bitmap->addPosition(pos);
						strShingle = parser->getShingle();
						
						delete charShingle;
					}

					bitmap->persist();
					documentos++;
					
					delete parser;
					delete bitmap;
				}
			}
		}
		closedir(directory);
        int shingleListSize = shinglesList->size();
        shinglesList->setInitialSize(shingleListSize);
		shinglesList->persistShingleList();
        printf ("\nLista de shingles persistida!\n Hay %d shingles\n", shingleListSize);
		delete shinglesList;
		delete v_stopWords;
		
		printf("\nCalculando secuencias randoms...\n");
		vector<SecuenciaRandom> v_secRand;
		for (int i=0; i<CANT_SECUENCIAS; i++) {
            SecuenciaRandom sr (shingleListSize, i);
			v_secRand.push_back(sr);
		}

		printf("Obteniendo bitmaps...\n");
		vector<Bitmap> v_bitmap;
		DIR* dir = opendir(REFERENCIA_CARPETA_CLUSTERS);
		struct dirent* entryB;
		while (entryB = readdir(dir)) {
			if ((strcmp(entryB->d_name, ".") != 0) && (strcmp(entryB->d_name, "..") != 0)) {
				if (strstr(entryB->d_name, EXTENSION_BITMAP)) {
					int tam = 256;
					char n_name[tam];
					char c [sizeof(REFERENCIA_CARPETA_CLUSTERS) + 1];
					strcpy (c, REFERENCIA_CARPETA_CLUSTERS);
					getFileName(n_name, c, entryB->d_name, tam);
					Bitmap bitmap (n_name);
					bitmap.loadBitmap();
					v_bitmap.push_back(bitmap);
				}
			}
		}

		printf("Obteniendo hashmins...\n");
		vector<Hashmin> v_hashmin;
		for (unsigned int i=0 ; i<v_bitmap.size(); i++) {
			Hashmin hashmin (v_secRand, v_bitmap.at(i));
			v_hashmin.push_back(hashmin);
		}

		bool multiplesOcurrencias = false;
		if (*ocur == 'N') {
			printf("Utilizando una ocurrencia por documente\n");
		}
		else if (*ocur == 'Y'){
			multiplesOcurrencias = true;
			printf("Utilizando multiples ocurrencias por documente\n");
		} else {
			printf ("Opcion '-o' incorrecta.\nSe utilizara por defecto 1 ocurrencia por documento\n");
		}

		printf("Clusterizando %d documentos...\n", documentos);
		if (cant >= documentos) {
			cant = -1;
			printf("Cantidad de clusters incorrecta, se utilizara default\n");
		}
		Kmeans* kmeans = new Kmeans(v_hashmin, cant);
		std::vector<Cluster> clusters;
		clusters = kmeans->clusterize(KMEANS_MAX_ITERATIONS, multiplesOcurrencias);

		string cFile = REFERENCIA_CARPETA_CLUSTERS CLUSTER_FILE;
		FILE* fd = fopen (cFile.c_str(), "w");
		ClusterPersist::persist(fd, clusters);
        printf ("\nHecho\n");

	} else if (opciones->size() == 1) {
		switch (opciones->at(0)) {
		case LIST:
			printListados(LIST);
			//printf("pase\n");
			break;
		case GROUPS:
			printListados(GROUPS);
			break;
		case APPEND:
			printf("Clasificar documento: %s\n", buffer);
			clasificar(buffer);
			break;
		case '?':
			printf("Parametros incompletos\n\n");
			break;
		default:
			break;
		}
	} else {
		if (opciones->size() > 3)
			printf("Demasiados parámetros\n\n");
		else if (opciones->size() == 0)
			printf("No ha elegido ninguna opción\n\n");
	}

	opciones->clear();
	delete opciones;
	return EXIT_SUCCESS;
}

void ShellParser::help() {
	printf("\norga\t\t\t\t\t\t\tUser Commands"
			"\n\nNOMBRE:"
			"\n\torga - Clusteriza archivos y clasifica."

			"\n\nDESCRIPCION: "
			"\n\tListado de comandos para utilizar este programa"
			"\n\n\t-h o --help "
			"\n\t\tMuestra esta ayuda y termina"
			"\n\n\t-d o --dir [directorio raiz]"
			"\n\t\tCrea los clusters dentro del directorio raíz"
			"\n\t\t[directorio raiz] "
			"\n\n\t-a o --append [archivo]"
			"\n\t\tClasifica el texto pasado como parámetro"
			"\n\t\t[archivo] Path del archivo"

			"\n\n\t-c o --cantidad [cantidad clusters]"
			"\n\t\tCantidad de clusters en los que se desea dividir los archivos"

			"\n\n\t-o o --ocurrences [opcion]"
			"\n\t\tIndica si un archivo puede pertenecer a distintos clusters o solo uno"
			"\n\t\t[opcion] Puede ser 'Y' o 'N'"

			"\n\n\t-l o --list"
			"\n\t\tLista los documentos del repositorio indicando al cluster al que corresponden"
			
			"\n\n\t-g o --groups"
			"\n\t\tLista los diferentes clusters y sus respectivos documentos"
			
			"\n\nEJEMPLOS: "
			"\n\tAlgunos ejemplos sobre como usar el programa"
			"\n\n\t./orga --help"
			"\n\n\t./orga --dir \"Directorio raiz\" --ocurrences \"Y\""
			"\n\n\t./orga --dir \"Directorio raiz\" --cantidad \"12\" --ocurrences \"N\""
			"\n\n\t./orga --list"
			"\n\n\t./orga -g"
			"\n"
	);
}

ShellParser::~ShellParser() {
}
