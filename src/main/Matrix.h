
#ifndef MATRIX_H_
#define MATRIX_H_

#include <vector>
#include <algorithm>    // std::random_shuffle
#include <cstdlib>      // std::rand, std::srand

using namespace std;

class Matrix {
public:
	Matrix();
	~Matrix();
	void initialize(int s, int k);
	vector <vector<int> > getMatrix();

private:
    vector <vector<int> > matrix;
    void sortRandomVector(vector<int> &myVector, int i);
};

#endif
