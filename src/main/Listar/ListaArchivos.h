
#ifndef LISTAARCHIVOS_H_
#define LISTAARCIVOS_H_

#include "../clustering/clusterpersist.h"
#include "../hashmin/hashmin.h"
#include <string.h>
#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <sstream>

using namespace std;

struct archivoItem {
  string archivo;
  int grupo;
};


class  ListaArchivos{
public:
	ListaArchivos();
	~ListaArchivos();
	void initialize(vector<Cluster>);
	void leerClustersCorrido(vector<Cluster>);
	void leerDocumentosOrdenados(vector<Cluster>);

private:
    vector<archivoItem> listaArchivos;
};


#endif
