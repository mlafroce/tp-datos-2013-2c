#include "ListaArchivos.h"
#include <cstdio>

ListaArchivos::ListaArchivos(){}
ListaArchivos::~ListaArchivos(){}

void ListaArchivos::initialize(vector<Cluster> listaCluster){
	int tam = listaCluster.size();
	vector <Hashmin> auxHashmin;
	stringstream sstm;
	string buffer = sstm.str();
	for (int i = 0; i < tam; i++) {
		auxHashmin = listaCluster.at(i).getHashmins();
		int cantHashmin = auxHashmin.size();
		if (cantHashmin == 0)
			cout << "No hay hashmins en ese grupo de cluster" << std::endl;
		else {
			for (int j = 0; j < cantHashmin; j++) {
				archivoItem auxArchivo;
				auxArchivo.grupo = i;
				auxArchivo.archivo = auxHashmin.at(j).name;
				listaArchivos.push_back(auxArchivo);
			}
		}
	}
}

void ListaArchivos::leerClustersCorrido(std::vector<Cluster> listaCluster) {
	int tam = listaCluster.size();
	vector <Hashmin> auxHashmin;
	for (int i = 0; i < tam; i++) {
		cout << "Grupo: " << i << endl;
		auxHashmin = listaCluster.at(i).getHashmins();
		int cantHashmin = auxHashmin.size();

		if (cantHashmin == 0)
			cout << "No hay hashmins en ese grupo de cluster" << endl;
		else {
			for (int j = 0; j < cantHashmin; j++) {
				int tamClu = sizeof(REFERENCIA_CARPETA_CLUSTERS);
				int tamName = auxHashmin.at(j).name.length();
//				cout << auxHashmin.at(j).name << " : name" << endl;
				cout << auxHashmin.at(j).name.substr(tamClu, tamName - (tamClu + 8)) << endl;
			}
		}
		cout << "" << endl;
	}
}


int cmp(const void* arg1, const void* arg2) {
	return strcmp(((archivoItem*)(arg1))->archivo.c_str(), ((archivoItem*)(arg2))->archivo.c_str());
}

void ListaArchivos::leerDocumentosOrdenados(vector<Cluster> vector){
	qsort((void*)(&listaArchivos.at(0)), listaArchivos.size(), sizeof(archivoItem), cmp);

	cout << "Los archivos son:\n" << endl;
	for (unsigned int i=0; i<listaArchivos.size(); i++) {
		int tamClu = sizeof(REFERENCIA_CARPETA_CLUSTERS);
		int tamName = listaArchivos.at(i).archivo.length();
		string name = listaArchivos.at(i).archivo.substr(tamClu, tamName - (tamClu + 8));
		printf ("%s - %d\n", name.c_str(), listaArchivos.at(i).grupo);
	}
}
