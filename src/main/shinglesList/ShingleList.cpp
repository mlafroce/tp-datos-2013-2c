/*
 * ShingleList.cpp
 *
 *  Created on: 05/11/2013
 *      Author: rodrigo
 */

#include "ShingleList.h"
#include <sstream>
#include <string.h>
#include "../Utils/Constantes.h"

ShingleList::ShingleList() {
    shingleCount = 0;
}

ShingleList::~ShingleList() {
	//shinglePositionMap.~map();
	//positionShingleMap.~map();
}

const std::map<int, int>& ShingleList::getPositionShingleMap() const {
	return positionShingleMap;
}

void ShingleList::setPositionShingleMap(const std::map<int, int>& positionShingleMap) {
	this->positionShingleMap = positionShingleMap;
}

int ShingleList::getShingleCount() const {
	return shingleCount;
}

void ShingleList::setShingleCount(int shingleCount) {
	this->shingleCount = shingleCount;
}

const std::map<int, int>& ShingleList::getShinglePositionMap() const {
	return shinglePositionMap;
}

void ShingleList::setShinglePositionMap(const std::map<int, int>& shinglePositionMap) {
	this->shinglePositionMap = shinglePositionMap;
}

int ShingleList::addShingle(char shingle[]) {
  int key = 0;
  key = CityHash32(shingle, sizeof(shingle));	
  int keyExists = shinglePositionMap.count(key);
  if (keyExists > 0) {
    return shinglePositionMap[key];
  } else {
    shinglePositionMap.insert(std::pair<int,int> (key,shingleCount));
    positionShingleMap.insert(std::pair<int,int> (shingleCount, key));
    shingleCount++;
    return shingleCount - 1;
  }
}

int ShingleList::getShingleAtPosition(int position) {
  std::map<int, int>::iterator it;
  it = this->positionShingleMap.find(position);
  if (it != this->positionShingleMap.end()) {
    return it->second;
  } else {
    return -1;
  }
}

int ShingleList::getShinglePosition(char* shingle) {
  std::map<int, int>::iterator it;
  int key = CityHash32(shingle, sizeof(shingle));
  it = this->shinglePositionMap.find(key);
  if (it != this->shinglePositionMap.end()) {
    return it->second;
  } else {
    return -1;
  }
}

void ShingleList::persistShingleList() {
	FILE* fd;
	string fileName = "ShingleList.data";
	string path = REFERENCIA_CARPETA_CLUSTERS + fileName;
	fd = fopen(path.c_str(), "w");
	if (!fd) {
		std::cout << "No se pudo abrir: " << path << std::endl;
		return;
	}
	for (std::map<int, int>::const_iterator i =	this->getShinglePositionMap().begin(); i != this->getShinglePositionMap().end(); ++i) {
		shingleListItem listItem;
		listItem.key = i->first;
		listItem.value = i->second;
		std::stringstream sstm;
		sstm << listItem.key << ":" << listItem.value;
		string buffer = sstm.str();
		int length = buffer.length();
		fwrite(&length, sizeof(int), 1, fd);
		fwrite(buffer.c_str(), sizeof(char), length, fd);
	}
	fclose(fd);
}


void ShingleList::loadShingleList() {
	shingleListItem listItem;
//	char* line;
	FILE* fd;
	string fileName = "ShingleList.data";
	string path = REFERENCIA_CARPETA_CLUSTERS + fileName;
	fd = fopen(path.c_str(), "r");
	if (!fd) {
		std::cout << "no pudo abrir shingleList.data" << std::endl;
		return;
	}

	this->shinglePositionMap.clear();
	this->positionShingleMap.clear();
	int length;
	while (fread(&length, sizeof(int), 1, fd)) {
		char line[length];
		fread(line, sizeof(char), length, fd);
		listItem.key = atoi(strtok(line, ":"));
		listItem.value = atoi(strtok(NULL, ":"));
		positionShingleMap.insert(std::pair<int, int>(listItem.value, listItem.key));
		shinglePositionMap.insert(std::pair<int, int>(listItem.key, listItem.value));
	}
	this->shingleCount = positionShingleMap.size();
}

int ShingleList::size()
{
	return this->shingleCount;
}

int ShingleList::getInitialSize()
{
	return this->initialSize;
}

void ShingleList::setInitialSize(int size)
{
	this->initialSize = size;
}
