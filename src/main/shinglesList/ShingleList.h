/*
 * ShingleList.h
 *
 *  Created on: 05/11/2013
 *      Author: rodrigo
 */

#ifndef SHINGLELIST_H_
#define SHINGLELIST_H_
#include "stdlib.h"
#include <map>
#include "../hashing/cityhash-1.1.1/src/city.h"
#include <iostream>
#include <cstdio>

#define MAXSHINGLE 4294967296

typedef struct shingleListItem {
  int key;
  int value;
} shingleListItem;
using namespace std;

class ShingleList {
private:
    std::map<int, int> shinglePositionMap;
	std::map<int,int> positionShingleMap;
	int shingleCount;
	int initialSize;

public:
	ShingleList();
	virtual ~ShingleList();
	/*
	 * * Getters and Setters
	 */
	const std::map<int, int>& getPositionShingleMap() const;
	void setPositionShingleMap(const std::map<int, int>& positionShingleMap);
	int getShingleCount() const;
	void setShingleCount(int shingleCount);
	const std::map<int, int>& getShinglePositionMap() const;
	void setShinglePositionMap(const std::map<int, int>& shinglePositionMap);

	int addShingle(char shingle[]);
	int getShingleAtPosition(int position);
	int getShinglePosition(char* shingle);
	void persistShingleList();
	void loadShingleList();
	int size();
	void setInitialSize(int size);
	int getInitialSize();
};

#endif /* SHINGLELIST_H_ */
