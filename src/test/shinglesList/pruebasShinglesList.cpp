#include "pruebasShinglesList.h"

void PruebasShinglesList::pruebaShinglesList()
{
  ShingleList testList;
  std::cout << "Iniciando pruebas de Shingles List" << std::endl;

  char * clave = (char*) "Ramon";
  int posRamon = testList.addShingle(clave);

  if (testList.getPositionShingleMap().size() == 1) {
    std::cout << "1) OK" << std::endl;
  }

  char* claveNueva = (char*) "Kiko";
  testList.addShingle(claveNueva);
  if (testList.getPositionShingleMap().size() == 2) {
    std::cout << "2) OK" << std::endl;
  }

  testList.addShingle(clave);
  if (testList.getPositionShingleMap().size() == 2) {
    std::cout << "3) OK" << std::endl;
  }

  int key = CityHash32(clave,sizeof(clave));
  if (key == testList.getShingleAtPosition(posRamon)) {
    std::cout << "4) OK" << std::endl;
  }

  if (testList.addShingle(clave) == posRamon) {
    std::cout << "5) OK" << std::endl;
  }
  
  testList.persistShingleList();
  ShingleList loadedList;
  loadedList.loadShingleList();
  if (loadedList.addShingle(clave) == posRamon) {
      std::cout << "6) OK" << std::endl;
  }

  if (key == loadedList.getShingleAtPosition(posRamon)) {
      std::cout << "7) OK" << std::endl;
  }

  std::cout << "Completadas las 5 pruebas, el tamaño de la lista es de " << testList.getPositionShingleMap().size() << std::endl;
}
