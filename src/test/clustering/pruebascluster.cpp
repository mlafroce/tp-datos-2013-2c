#include <iostream>
#include <vector>
#include "pruebascluster.h"
#include "../../main/hashmin/bitmap.h"
#include "../../main/clustering/kmeans.h"

Bitmap getTestBitmap(int seed, const char* name, int size);

void PruebasCluster::pruebaCluster()
{
	std::cout << "creando secuencias" << std::endl;
	std::vector<SecuenciaRandom> v_secRand;
	for (int i=0; i<100; i++) {
		SecuenciaRandom sr (10000, i);
		v_secRand.push_back(sr);
	}
	std::vector<Hashmin> hashmines;
	std::cout << "Creando hashmines" << std::endl;
	Bitmap bm = getTestBitmap(1, "bm", 10000);
	std::cout << "El primer BM tiene tamaño " << bm.size() << std::endl;
	Hashmin hm(v_secRand, bm);
	hashmines.push_back(hm);
	//Hashmin 2
	bm = getTestBitmap(2, "bm2", 10000);
	Hashmin hm2(v_secRand, bm);
	hashmines.push_back(hm2);
	//Hashmin 3
	bm = getTestBitmap(10, "bm10", 10000);
	Hashmin hm10(v_secRand, bm);
	hashmines.push_back(hm10);
	//4
	bm = getTestBitmap(26, "bm26", 10000);
	Hashmin hm26(v_secRand, bm);
	hashmines.push_back(hm26);
	//4
	bm = getTestBitmap(39, "bm39", 10000);
	Hashmin hm39(v_secRand, bm);
	hashmines.push_back(hm39);
	//4
	bm = getTestBitmap(30, "bm30", 10000);
	Hashmin hm30(v_secRand, bm);
	hashmines.push_back(hm30);
	std::cout << "clusterizando 2 grupos" << std::endl;
	Kmeans kmeans(hashmines, 2);
	std::vector<Cluster> clusters = kmeans.clusterize(1000);
	std::cout << "clusterizado" << std::endl;
}

Bitmap getTestBitmap(int seed, const char* name, int size)
{
	Bitmap bm(name);
	int i;
	switch (seed){
		case 1:
			for (i=0; i < size; i+= 11){
				bm.addPosition(i);
				bm.addPosition(i+5);
			}
			break;
		case 2:
			for (i=0; i < size; i+= 13){
				bm.addPosition(i);
				bm.addPosition(i+7);
			}
			break;
		default:
			for (i=0; i < size; i+= seed){
				bm.addPosition(i);
			}
			break;
		return bm;
	}
}