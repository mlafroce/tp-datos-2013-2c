#include "pruebas.h"
#include "hashmin/pruebashashmin.h"
#include "hashmin/pruebasbitmap.h"
#include "clustering/pruebascluster.h"
#include "shinglesList/pruebasShinglesList.h"
#include <iostream>

void Pruebas::correrPruebas()
{
	std::cout << "Pruebas bitmap" << std::endl;
	PruebasBitmap pb;
	pb.pruebaBitmap();
	std::cout << "Pruebas hashmin" << std::endl;
	PruebasHashmin ph;
	ph.pruebaSecuencia();
	PruebasCluster pc;
	std::cout << "Pruebas de cluster" << std::endl;
	pc.pruebaCluster();
	ph.pruebaMinhash();
	std::cout << "Pruebas de Lista de Shingles" << std::endl;
	PruebasShinglesList psh;
	psh.pruebaShinglesList();
}
