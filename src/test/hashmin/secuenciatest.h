#ifndef SECUENCIATEST_H
#define SECUENCIATEST_H

#include "../../main/hashmin/secuenciarandom.h"

/**
 * Esta clase la uso para hacer pruebas con secuencias prefijadas
 */
class SecuenciaTest : public SecuenciaRandom
{
public:
	SecuenciaTest(int size, int seed);
// 	SecuenciaTest(SecuenciaTest &secuenciaTest);
};

#endif // SECUENCIATEST_H
