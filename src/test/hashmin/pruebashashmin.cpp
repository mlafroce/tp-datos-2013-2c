#include "pruebashashmin.h"
#include "secuenciatest.h"
#include "../../main/hashmin/secuenciarandom.h"
#include "../../main/hashmin/bitmap.h"
#include "../../main/hashmin/hashmin.h"
#include <iostream>

std::vector<SecuenciaRandom> generarSecuencias(){
	std::vector<SecuenciaRandom> secuencias;
	SecuenciaTest s1(10,0);
	SecuenciaTest s2(10,1);
	SecuenciaTest s3(10,2);
	secuencias.push_back(s1);
	secuencias.push_back(s2);
	secuencias.push_back(s3);
	return secuencias;
}

void PruebasHashmin::pruebaSecuencia()
{
	SecuenciaRandom otraSec(20, 0);
	while (otraSec.hasNext()){
		std::cout << "Número rand : " << otraSec.getNext() << std::endl;
	}	
	std::cout << "___Secuencia 1:___ " << std::endl;
	// OJO, No hay constructor copia:
	SecuenciaTest sr(10, 1);
	while (sr.hasNext()){
		std::cout << "Número: " << sr.getNext() << std::endl;
	}
}

void PruebasHashmin::pruebaMinhash()
{
	Bitmap bm("PruebaMinhash.bit");
	bm.addPosition(6);
	std::vector<SecuenciaRandom> secs = generarSecuencias();
	std::cout << "secsize: " << secs.size() << std::endl;
	Hashmin hashmin(secs, bm);
	std::cout << "___Prueba bitmap:___ " << std::endl;
	// OJO, No hay constructor copia:
	int i = 0;
	while (i < hashmin.values.size()){
		std::cout << "Número: " << hashmin.values.at(i) << std::endl;
		i++;
	}
}