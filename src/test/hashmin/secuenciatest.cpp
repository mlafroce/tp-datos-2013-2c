#include "secuenciatest.h"
#include <iostream>

SecuenciaTest::SecuenciaTest(int size, int seed) : SecuenciaRandom(size, seed)
{
	const int myInts[] = {0,2,4,6,1,3,5};
	const int myInts2[] = {0,1,3,2,4,5,6};
	const int myDefInts[]= {0,1,2,3,4,5,6};
	switch (seed){
		case 1:
			this->seq.assign(myInts, myInts+7);
			break;
		case 2:
			this->seq.assign(myInts2, myInts2 + 7);
			break;
		default:
			this->seq.assign(myDefInts, myDefInts+7);
			break;
	}
	this->it = this->seq.begin();
}