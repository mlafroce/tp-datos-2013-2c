#include "pruebasbitmap.h"
#include "../../main/hashmin/bitmap.h"
#include <iostream>
#include <cstring>

void PruebasBitmap::pruebaBitmap()
{
	Bitmap bm("Prueba");
	std::cout << "Iniciando bits" << std::endl;
	bm.addPosition(2);
	bm.addPosition(100);
	bm.addPosition(9);
	std::cout << "Bits inicializados , el tamanio del bitmap es de " << bm.size() << std::endl;
	if (!bm.getPosition(0))
		std::cout << "1) ok" << std::endl;
	if (bm.getPosition(2))
		std::cout << "2) ok" << std::endl;
	if (bm.getPosition(100))
		std::cout << "3) ok" << std::endl;
	if (!bm.getPosition(12345678))
		std::cout << "4) ok" << std::endl;
	std::cout << "Las 4 pruebas terminaron, el tamanio del bitmap es de " << bm.size() << std::endl;
	bm.persist();
	std::cout << "el mapa fue persistido" << std::endl;
	Bitmap bitmapLoad("Prueba");
	bitmapLoad.loadBitmap();
	if (strcmp(bitmapLoad.getName().c_str(), (char*) "Prueba") == 0) {
	  std::cout << "El archivo abierto es el correcto" << std::endl;
	}
	if (bitmapLoad.size() == bm.size()) {
	  std::cout << "el tamaño del archivo es el mismo" << std::endl;
	} else {
	  std::cout << "el tamaño no coincide :( el bm es de" << bm.size() << " el otro es de " << bitmapLoad.size() << std::endl;
	}
	
	
}
